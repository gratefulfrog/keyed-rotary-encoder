#include "HWDebouncedKeyedRotaryEncoder.h"

HWDKRE *HWDKRE::instancePtr;

void HWDKRE::fallingS1_ptr(HWDKRE *instancePtr){ 
  instancePtr->_count += digitalRead(instancePtr->_s2Pin) ? 1 : -1;
}

void HWDKRE::fallingS1(){ 
  HWDKRE::fallingS1_ptr(HWDKRE::instancePtr);	
}	
void HWDKRE::keyPressed(){
  attachInterrupt(HWDKRE::instancePtr->_s1_InterruptID, HWDKRE::fallingS1, FALLING); 
  attachInterrupt(HWDKRE::instancePtr->_key_InterruptID, HWDKRE::keyReleased, RISING); 
} 
void HWDKRE::keyReleased(){
  detachInterrupt(HWDKRE::instancePtr->_s1_InterruptID);
  attachInterrupt(HWDKRE::instancePtr->_key_InterruptID, HWDKRE::keyPressed, FALLING); 
}									
  
HWDKRE::HWDKRE(byte s1Pin,byte s2Pin,byte keyPin):
  _s1_InterruptID(digitalPinToInterrupt(s1Pin)),
  _s2Pin(s2Pin),
  _key_InterruptID(digitalPinToInterrupt(keyPin)){

  _count     = 0;
  _fullCount = 0;

  pinMode(s1Pin,  INPUT);  //no pulling since the encoder does this!
  pinMode(s2Pin,  INPUT);
  pinMode(keyPin, INPUT);

  HWDKRE::instancePtr = this;
  attachInterrupt(_key_InterruptID,HWDKRE::keyPressed, FALLING);
}

bool HWDKRE::update(int &countReturned){
  // set countReturned to total of new counts since last call to update!
  bool res = false;
  int tempCount = _count;
  if (_fullCount !=tempCount){
    countReturned = _fullCount - tempCount;
    _fullCount = tempCount;
    res = true;
  }
  return res;
}
