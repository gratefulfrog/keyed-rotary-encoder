#ifndef HWDebouncedKeyedRotaryEncoder_H
#define HWDebouncedKeyedRotaryEncoder_H

#include <Arduino.h>

class HWDKRE;

class HWDKRE{
  public:

    static HWDKRE *instancePtr;
    
    static void fallingS1_ptr  (HWDKRE *instancePtr); 
    static void fallingS1();
    static void keyPressed();
    static void keyReleased();			
    
  protected:
    volatile int _count;
    int          _fullCount;
    const byte   _s1_InterruptID,
                 _s2Pin,
                 _key_InterruptID;
    
  public:
    HWDKRE(byte s1Pin,byte s2Pin,byte keyPin);
    bool update(int &countReturned);  // sets countReturned to the new counts since last update
};

#endif
