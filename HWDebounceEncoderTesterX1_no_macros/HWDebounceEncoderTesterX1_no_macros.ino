
#include "HWDebouncedKeyedRotaryEncoder.h"

const byte nbEnc = 1;

HWDKRE *encVec[nbEnc];

// MKR Arduino has 10 Exernal Interrupt Pins: 0,1,4,5,6,7,8,9,A1,A2

const byte pinArray[5][3] = {{ 0, 2, 1}}; //S1,S2,Key : S1 & Key must be available for external interrupt
                             /*{A1,A3,A2},
                             { 4, 3, 5},
                             { 6,10, 7},
                             { 8,13, 9}};  // note that pins 11 & 12 are not useable as S2 in this context! 
                                           // maybe because they are I2C SDA and SCL and are pulled up?
                             */
void instanciateEncoders(){
  for (byte i=0;i<nbEnc;i++){
    encVec[i] = new HWDKRE(pinArray[i][0],pinArray[i][1],pinArray[i][2]);
  }
}

void setup() {
  Serial.begin(115200);
  while(!Serial);
  instanciateEncoders();
  Serial.println("Starting up!");
}

bool checkUpdates(int resVec[]){
  bool res = false;
  for (byte i=0;i<nbEnc;i++){
    int cL=0;
    res = encVec[i]->update(cL) || res;
    resVec[i] +=cL;
  }
  return res;
}

void showVec (int vec[]){
  for (byte i=0;i<nbEnc;i++){
    Serial.print(String(vec[i]) + (i<(nbEnc-1) ? "  :  " : "\n"));
  }
}

void loop() {
  static int countVec[] ={0,0,0,0,0};
  if (checkUpdates(countVec)){
    showVec(countVec);
  }
}
