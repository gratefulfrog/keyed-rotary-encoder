# Keyed Rotary Encoder

Have you been fighting with bounce and rotary encoders in your Arduino project?

Well I have!

This [little video](https://youtu.be/Hw0OoGYii9o) shows how to use [Keyed Rotary Encoders](https://www.aliexpress.com/item/32915420023.html) in Arduino Code.

The idea is that when the key is pressed, the arduino will start counting encoder ticks, positive clockwise, or negative counter-clockwise. The display is updated on change in counts.

Since these encoders have built in hardware debouncing, no need to play around with delays and so on.

The code uses the C-preprocessor to generate method definitions for the 10 sets of interrupt handlers needed.

For a given encoder:
1. set an interrupt handler to fire on key pin falling edge:
    this handler sets an interrupt handler on falling edge for the encoder S1 pin, as well as a new handler for the rising edge of key pin.
2. the S1 falling edge handler looks at S2 and increments or decrements the tick count,
3. the risinge edge key pin handler unsets the handler on S1  and resets the falling edge handler fo the key pin.

The 5 encoder code is available in the repo. It makes extensive use of the C-preprocessor to generate the multiple method
definitions needed. I am sincerly sorry to have to resort to such ugly syntax...

A simple single encoder version is also available in the repo.

I hope this helps someone!
